

var qiang_mi3 = {
	'start': function() {
		var buy_phone_btn = document.getElementById('buy_phone_btn');
		if (buy_phone_btn) {
			var control_btn = document.getElementById('control_btn');
			if (control_btn.getAttribute('qiang') == 'true') {
				var control_btn = document.getElementById('control_btn')
				control_btn.innerHTML = '正在抢手机中';
				control_btn.setAttribute('qiang', 'true');
				qiang_mi3.starting = true;
				buy_phone_btn.click();
				document.getElementById('modal-body').className = 'modal-body loading';
			}
		}
	},
	'iframe_load': function() {
		document.getElementById('modal-body').className = 'modal-body';
		var control_btn = document.getElementById('control_btn');
		if (control_btn.getAttribute('qiang') == 'true') {
			setTimeout(qiang_mi3.start, 100);
		}
	},
	'fix_btn': function() {
		var btns_div = document.getElementById('hdBtns');
		var btns = btns_div.childNodes;
		for (var i=0; i<btns.length; i++) {
			var btn = btns[i];
			if (btn.innerHTML == '支付手机' || btn.innerHTML == '购买手机') {
				btn.href = 'http://t.hd.xiaomi.com/r/?_a=payment_check';
				btn.id = 'buy_phone_btn';
				btn.target = 'buy_phone_iframe';
				break;
			}
		}
	},
	'add_iframe': function() {
		var status_div = document.createElement('div');
		status_div.className = 'modal';
		
		var status_header = document.createElement('div');
		status_header.className = 'modal-header';
		
		var control_btn = document.createElement('a');
		control_btn.id = 'control_btn';
		control_btn.className = 'control_btn';
		control_btn.innerHTML = '开始自动购买';
		control_btn.style.float = 'right';
		control_btn.href = 'javascript:;';
		status_header.appendChild(control_btn);

		var status_header_h3 = document.createElement('h3');
		status_header_h3.innerHTML = '购买结果';
		status_header.appendChild(status_header_h3);

		status_div.appendChild(status_header);

		var status_body = document.createElement('div');
		status_body.className = 'modal-body';
		status_body.id = 'modal-body';

		var hidden_iframe = document.createElement('iframe');
		hidden_iframe.id = 'buy_phone_iframe';
		hidden_iframe.name = 'buy_phone_iframe';
		hidden_iframe.frameBorder = '0';

		status_body.appendChild(hidden_iframe);
		
		status_div.appendChild(status_body);
		document.body.appendChild(status_div);

		hidden_iframe.addEventListener('load', qiang_mi3.iframe_load);
		control_btn.setAttribute('qiang', 'false');
		control_btn.addEventListener('click', qiang_mi3.control_btn);
	},
	'stop': function() {
		var control_btn = document.getElementById('control_btn');
		control_btn.innerHTML = '开始自动购买';
	},
	'control_btn': function(){
		var control_btn = document.getElementById('control_btn');
		if (control_btn.getAttribute('qiang') == 'true') {
			control_btn.setAttribute('qiang', 'false');
			qiang_mi3.stop();
		}
		else {
			control_btn.setAttribute('qiang', 'true');
			qiang_mi3.start();
		}
		return false;
	}
};

(function(){
	qiang_mi3.fix_btn();
	qiang_mi3.add_iframe();
})()


